package tsc.abzalov.tm.command.auth;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputPassword;

@SuppressWarnings("unused")
public final class AuthChangePasswordCommand extends AbstractCommand {

    public AuthChangePasswordCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "change-password";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change current user password.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("PASSWORD CHANGING");
        @NotNull val newPassword = inputPassword();
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val authService = getServiceLocator().getAuthService();
        @NotNull val currentUserId = authService.getCurrentUserId();
        @NotNull val userService = getServiceLocator().getUserService();
        @Nullable var updatedUser = userService.editPasswordById(currentUserId, newPassword);
        Optional.ofNullable(updatedUser).orElseThrow(() -> new UserIsNotExistException(currentUserId));
        System.out.println("Password successfully changed.\n");
    }

}
