package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

public interface IAuthService {

    void register(@Nullable String login, @Nullable String password, @Nullable String firstName,
                  @Nullable String lastName, @Nullable String email);

    void login(@Nullable String login, @Nullable String password);

    void logoff();

    boolean isSessionInactive();

    @NotNull
    String getCurrentUserId();

    @NotNull
    String getCurrentUserLogin();

    @NotNull
    Role getCurrentUserRole();

}
