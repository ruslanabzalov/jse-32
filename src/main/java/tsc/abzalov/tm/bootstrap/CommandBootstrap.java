package tsc.abzalov.tm.bootstrap;

import lombok.Getter;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.*;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.component.AbstractBackgroundTaskComponent;
import tsc.abzalov.tm.component.BackupComponent;
import tsc.abzalov.tm.component.CommandsFileScannerComponent;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.repository.UserRepository;
import tsc.abzalov.tm.service.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.util.InputUtil.INPUT;
import static tsc.abzalov.tm.util.SystemUtil.getApplicationPid;

@Getter
public final class CommandBootstrap implements IServiceLocator {

    @NotNull
    private final IApplicationPropertyService propertyService = new ApplicationPropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final AbstractBackgroundTaskComponent backupComponent = new BackupComponent(this);

    @NotNull
    private final AbstractBackgroundTaskComponent commandsFileScannerComponent =
            new CommandsFileScannerComponent(this);

    {
        registerCommands();
        propertyService.initLocalProperties();
        registerApplicationPid();

        try {
            @NotNull val adminLogin = "admin";
            @NotNull val adminPassword = "admin";
            @NotNull val adminRole = ADMIN;
            @NotNull val adminFirstname = "Admin";
            @NotNull val adminLastname = "";
            @NotNull val adminEmail = "admin@mail.com";

            userService.create(adminLogin, adminPassword, adminRole, adminFirstname, adminLastname, adminEmail);

            @Nullable var admin = userService.findByLogin("admin");
            admin = Optional.ofNullable(admin).orElseThrow(UserIsNotExistException::new);

            @NotNull val adminProjectName = "Main Project";
            @NotNull val adminProjectDescription = "Admin Main Project";
            @NotNull val adminTaskName = "Main Task";
            @NotNull val adminTaskDescription = "Admin Main Task";

            initProject(adminProjectName, adminProjectDescription, admin.getId());
            initTask(adminTaskName, adminTaskDescription, admin.getId());

            @NotNull val testLogin = "test";
            @NotNull val testPassword = "test";
            @NotNull val testFirstname = "Test";
            @NotNull val testLastname = "Test";
            @NotNull val testEmail = "test@mail.com";

            userService.create(testLogin, testPassword, testFirstname, testLastname, testEmail);

            @Nullable var testUser = userService.findByLogin("test");
            testUser = Optional.ofNullable(testUser).orElseThrow(UserIsNotExistException::new);

            @NotNull val testUserProjectName = "Simple Project";
            @NotNull val testUserProjectDescription = "Test User Simple Project";
            @NotNull val testUserTaskName = "Simple Task";
            @NotNull val testUserTaskDescription = "Test User Simple Task";

            initProject(testUserProjectName, testUserProjectDescription, testUser.getId());
            initTask(testUserTaskName, testUserTaskDescription, testUser.getId());
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
        }
    }

    public void run(@NotNull final String... args) {
        System.out.println();
        if (areArgsExecuted(args)) return;

        loggerService.info("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");
        System.out.println("Please, \"login\" or \"register\" commands to start working with the application.\n");

        runBackgroundTasks();

        @NotNull val availableStartupCommands =
                Arrays.asList("register", "login", "help", "exit", "info", "about", "exit", "help", "version");
        @Nullable var commandName = "";
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            commandName = INPUT.nextLine();
            System.out.println();

            if (isEmpty(commandName)) continue;

            if (authService.isSessionInactive()) {
                inactiveSessionExecution(availableStartupCommands, commandName);
                continue;
            }

            activeSessionExecution(commandName);
        }
    }

    private void runBackgroundTasks() {
        backupComponent.run();
        commandsFileScannerComponent.run();
    }

    private boolean areArgsExecuted(@NotNull final String[] args) {
        try {
            if (areArgExists(args)) return true;
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
            return true;
        }
        return false;
    }

    private void inactiveSessionExecution(@NotNull final List<String> availableStartupCommands,
                                          @NotNull final String commandName) {
        val isCommandAvailable = availableStartupCommands.contains(commandName);

        if (!isCommandAvailable) {
            System.out.println("Session is inactive! Please, register new user or login.");
            return;
        }

        try {
            loggerService.command(commandName);
            @NotNull val command = commandService.getCommandByName(commandName);
            command.execute();
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
        }
    }

    private void activeSessionExecution(@NotNull final String commandName) {
        try {
            @NotNull val command = commandService.getCommandByName(commandName);
            @NotNull val commandRoles = command.getRoles();
            @NotNull val currentUserRole = authService.getCurrentUserRole();
            val canUserExecuteCommand = commandRoles.contains(currentUserRole);

            if (canUserExecuteCommand) {
                loggerService.command(commandName);
                command.execute();
                return;
            }

            throw new AccessDeniedException();
        } catch (@NotNull final Exception exception) {
            loggerService.error(exception);
        }
    }

    private void registerCommands() {
        commandService.initCommands(this);
    }

    private void initProject(@NotNull final String projectName, @NotNull final String projectDescription,
                             @NotNull final String userId) {
        @NotNull val project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setUserId(userId);

        projectService.create(project);
    }

    private void initTask(@NotNull final String taskName, @NotNull final String taskDescription,
                          @NotNull final String userId) {
        @NotNull val task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setUserId(userId);

        taskService.create(task);
    }

    private boolean areArgExists(@NotNull final String... args) {
        if (isEmpty(args)) return false;

        @NotNull val arg = args[0];
        if (isEmpty(arg)) return false;

        @NotNull val command = commandService.getArgumentByName(arg);
        command.execute();
        return true;
    }

    private void registerApplicationPid() {
        @NotNull val pidFileName = "task-manager.pid";
        @NotNull val pidFile = new File(pidFileName);
        @NotNull val pid = Long.toString(getApplicationPid());

        try {
            Files.write(Paths.get(pidFile.toURI()), pid.getBytes());
        } catch (@NotNull final IOException exception) {
            loggerService.error(exception);
        }

        pidFile.deleteOnExit();
    }

}
