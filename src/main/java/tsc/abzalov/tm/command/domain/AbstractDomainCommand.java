package tsc.abzalov.tm.command.domain;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.domain.Domain;

public abstract class AbstractDomainCommand extends AbstractCommand {

    @NotNull
    public static final String BINARY_FILENAME = "data.bin";

    @NotNull
    public static final String BASE64_FILENAME = "data.base64";

    @NotNull
    public static final String FASTERXML_XML_FILENAME = "data_fasterxml.xml";

    @NotNull
    public static final String FASTERXML_JSON_FILENAME = "data_fasterxml.json";

    @NotNull
    public static final String FASTERXML_YAML_FILENAME = "data_fasterxml.yml";

    @NotNull
    public static final String JAXB_XML_FILENAME = "data_jaxb.xml";

    @NotNull
    public static final String JAXB_JSON_FILENAME = "data_jaxb.json";

    @NotNull
    public static final String BACKUP_FILENAME = "backup.json";

    @NotNull
    public static final String JAXB_CONTEXT_FACTORY_PROPERTY_NAME = "javax.xml.bind.context.factory";

    @NotNull
    public static final String JAXB_CONTEXT_FACTORY_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String JAXB_MEDIA_TYPE = "application/json";

    public AbstractDomainCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public Domain getDomain() {
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val taskService = serviceLocator.getTaskService();
        @NotNull val userService = serviceLocator.getUserService();
        @NotNull val projects = projectService.findAll();
        @NotNull val tasks = taskService.findAll();
        @NotNull val users = userService.findAll();

        @NotNull val domain = new Domain();
        domain.setProjects(projects);
        domain.setTasks(tasks);
        domain.setUsers(users);
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;

        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val taskService = serviceLocator.getTaskService();
        @NotNull val userService = serviceLocator.getUserService();
        @NotNull val authService = serviceLocator.getAuthService();

        projectService.clear();
        projectService.addAll(domain.getProjects());

        taskService.clear();
        taskService.addAll(domain.getTasks());

        userService.clear();
        userService.addAll(domain.getUsers());

        if (authService.isSessionInactive()) return;
        authService.logoff();
    }

}
