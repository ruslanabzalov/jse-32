package tsc.abzalov.tm.component;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.SECONDS;

public final class CommandsFileScannerComponent extends AbstractBackgroundTaskComponent {

    public CommandsFileScannerComponent(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void run() {
        getScheduledExecutorService()
                .scheduleWithFixedDelay(this::scanCommandsFiles, INITIAL_DELAY, SCANNER_DELAY, SECONDS);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SneakyThrows
    private void scanCommandsFiles() {
        @NotNull val files = new File(COMMANDS_FILES_PATH).listFiles();
        if (files.length == 0) return;
        Arrays.sort(files, Comparator.comparingLong(File::lastModified));

        @NotNull val argumentsCommands =
                getServiceLocator().getCommandService().getArgumentsCommands();
        @NotNull val commandNames = argumentsCommands.stream()
                .map(AbstractCommand::getCommandName)
                .collect(Collectors.toList());
        @NotNull var fileName = "";
        for (@NotNull val file : files) {
            fileName = file.getName();
            if (commandNames.contains(fileName)) {
                executeCommand(fileName);
                file.delete();
            }
        }
    }

}
