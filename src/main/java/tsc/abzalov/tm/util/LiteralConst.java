package tsc.abzalov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class LiteralConst {

    @NotNull
    public static final String DEFAULT_NAME = "Name is empty";

    @NotNull
    public static final String DEFAULT_DESCRIPTION = "Description is empty";

    @NotNull
    public static final String IS_NOT_STARTED = "Is not started";

    @NotNull
    public static final String IS_NOT_ENDED = "Is not ended";

    @NotNull
    public static final String DEFAULT_REFERENCE = "Empty";

    @NotNull
    public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";

    @NotNull
    public static final String DEFAULT_LOGIN = "Login is empty";

    @NotNull
    public static final String DEFAULT_FIRSTNAME = "Firstname is empty";

    @NotNull
    public static final String DEFAULT_LASTNAME = "Lastname is empty";

    @NotNull
    public static final String DEFAULT_EMAIL = "Email is empty";

    @NotNull
    public static final String ACTIVE = "Active";

    @NotNull
    public static final String LOCKED = "Locked";

    @NotNull
    public static final String COMMANDS_PACKAGE = "tsc.abzalov.tm.command";

    @NotNull
    public static final String DEFAULT_APP_VERSION = "0.0.0";

    @NotNull
    public static final String DEFAULT_DEVELOPER_NAME = "Unknown";

    @NotNull
    public static final String DEFAULT_DEVELOPER_EMAIL = "email@mail.com";

    @NotNull
    public static final String DEFAULT_HASHING_SALT = "qwerty";

    @NotNull
    public static final String DEFAULT_HASHING_COUNTER = "10";

}
