package tsc.abzalov.tm.repository;

import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

import static tsc.abzalov.tm.util.LiteralConst.COMMANDS_PACKAGE;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getStartupCommands() {
        @NotNull val commands = this.commands.values();
        return commands.stream()
                .filter(this::isStartupCommand)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getArgumentsCommands() {
        @NotNull val commands = this.commands.values();
        return commands.stream()
                .filter(this::hasArgument)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        @NotNull val commands = getCommands();
        @NotNull val attributes = getAttributes(commands);
        return attributes;
    }

    @Override
    @NotNull
    public Collection<String> getCommandArguments() {
        @NotNull val arguments = this.arguments.values();
        @NotNull val attributes = getAttributes(arguments);
        return attributes;
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        @NotNull val command = commands.get(name);
        return command;
    }

    @Override
    @Nullable
    public AbstractCommand getArgumentByName(@NotNull final String name) {
        @NotNull val argument = arguments.get(name);
        return argument;
    }

    @Override
    @SneakyThrows
    public void initCommands(@NotNull final IServiceLocator serviceLocator) {
        @NotNull val reflections = new Reflections(COMMANDS_PACKAGE);
        @NotNull val classes = reflections.getSubTypesOf(AbstractCommand.class);

        var isAbstract = false;
        for (@NotNull val clazz : classes) {
            isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            add(clazz.getDeclaredConstructor(IServiceLocator.class).newInstance(serviceLocator));
        }
    }

    @NotNull
    private List<String> getAttributes(@NotNull final Collection<AbstractCommand> commands) {
        @NotNull val attributes = new ArrayList<String>();

        for (@NotNull val command : commands) {
            @Nullable val attribute = command.getCommandArgument();

            val isAttributeExist = Optional.ofNullable(attribute).isPresent();
            if (isAttributeExist) attributes.add(attribute);
        }

        return attributes;
    }

    private void add(@NotNull final AbstractCommand command) {
        @NotNull val name = command.getCommandName();
        @Nullable val argument = command.getCommandArgument();

        commands.put(name, command);

        val isArgumentExist = Optional.ofNullable(argument).isPresent();
        if (isArgumentExist) arguments.put(name, command);
    }

    private boolean isStartupCommand(@NotNull final AbstractCommand command) {
        @NotNull val commandName = command.getCommandName();
        @NotNull val availableStartupCommands = Arrays.asList("register", "login", "help", "exit");
        return availableStartupCommands.contains(commandName);
    }

    private boolean hasArgument(@NotNull final AbstractCommand command) {
        return command.getCommandArgument() != null;
    }

}
