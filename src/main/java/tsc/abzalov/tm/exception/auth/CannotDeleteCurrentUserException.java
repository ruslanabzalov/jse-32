package tsc.abzalov.tm.exception.auth;

import tsc.abzalov.tm.exception.AbstractException;

public final class CannotDeleteCurrentUserException extends AbstractException {

    public CannotDeleteCurrentUserException() {
        super("Cannot delete current user!");
    }

}
