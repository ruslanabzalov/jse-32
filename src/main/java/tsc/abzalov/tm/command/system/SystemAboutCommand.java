package tsc.abzalov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

@SuppressWarnings("unused")
public final class SystemAboutCommand extends AbstractCommand {

    public SystemAboutCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "about";
    }

    @Override
    @NotNull
    public String getCommandArgument() {
        return "-a";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Shows developer info.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val propertyService = serviceLocator.getPropertyService();
        @NotNull val developerName = propertyService.getDeveloperName();
        @NotNull val developerEmail = propertyService.getDeveloperEmail();
        @NotNull val manifestDeveloperName = Manifests.read("Developer");
        @NotNull val manifestDeveloperEmail = Manifests.read("Email");

        System.out.println("Developer Full Name: " + developerName);
        System.out.println("Developer Email: " + developerEmail);
        System.out.println("Manifest Developer Full Name: " + manifestDeveloperName);
        System.out.println("Manifest Developer Email: " + manifestDeveloperEmail + "\n");
    }

}
