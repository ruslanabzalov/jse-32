package tsc.abzalov.tm.command.domain;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.domain.Domain;
import tsc.abzalov.tm.enumeration.CommandType;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;

@SuppressWarnings("unused")
public final class DataBinaryLoadCommand extends AbstractDomainCommand {

    public DataBinaryLoadCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "data-binary-load";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from binary format.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull val fileInputStream = new FileInputStream(BINARY_FILENAME);
        @NotNull val objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull val domain = (Domain) objectInputStream.readObject();
        setDomain(domain);

        objectInputStream.close();
        fileInputStream.close();

        System.out.println("Data was loaded from binary format.\nPlease, re-login.\n");
    }

}
