package tsc.abzalov.tm.exception.auth;

import tsc.abzalov.tm.exception.AbstractException;

public final class SessionIsInactiveException extends AbstractException {

    public SessionIsInactiveException() {
        super("Session is already inactive! Please, login or register a new user.");
    }

}
